<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use DebugBar\Storage\StorageInterface;
use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;



class ImageController extends Controller
{
    public function index(Project $project){
        $project->load([
            'images' => function($q){
                $q->orderBy('featured', 'DESC');
            }
        ])->get();

        return view('images.index')->with(compact('project'));
    }
    public function store(Request $request){
        $success = true;
        $files = $request->file('file');
        $project_id = $request->input('project_id');

        foreach($files as $file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $thumb_fileName = 'thumb_'.$fileName;
            if ($imagePath = $file->storeAs('images', $fileName)) {
                $intervention = new ImageManager(['driver' => 'gd']);
                $image = $intervention->make(Storage::get($imagePath))->fit(1280, 1024)->encode();
                Storage::disk('public')->put('images/projects/' . $fileName, $image);
                Storage::disk('public')->delete('images/' . $fileName);
                DB::beginTransaction();
                try {
                    $product_image = Image::create([
                        'image' => $fileName,
                        'project_id' => $project_id
                    ]);
                } catch (\Exception $exception) {
                    $success = $exception->getMessage();
                    DB::rollBack();
                }
                if ($success === true) {
                    DB::commit();
                }
            }
        }
        if ($success === true) {
            return response()->json(['text' => __('imagenes registradas correctamente')]);
        }
        return response()->json(['text' => $success]);
    }

    public function destroy(Image $image){
        $success = true;
        try{
            DB::beginTransaction();
            $image->delete();

        }
        catch (\Exception $exception) {
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            if ($image->featured == true) {
                $product = $image->product;
                $image = $product->product_images->first();
                if ($image) {
                    $image->destacar($product->id);
                }
            }
            return back();
        }
        return back();
    }

    public function feature($project_id, $image_id){

        $product_image = Image::find($image_id);
        $product_image->destacar($project_id);

        return back();
    }


}
