<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Job;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    public function index(User $user){
        $user->load(['jobs' => function($q){
            $q->select('jobs.*')->orderBy('begin', 'DESC');
        }])->get();
        return view('jobs/index')->with(compact('user'));
    }

    public function store(JobRequest $request, User $user){
        $success = true;
        try{
            DB::beginTransaction();

            Job::create([
                'company' => $request->input('company'),
                'begin' => $request->input('begin'),
                'end' => $request->input('end'),
                'job' => $request->input('job'),
                'description' => $request->input('description'),
                'user_id' => $user->id
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text'=> __('Datos almacenados')]);
        }

        return response()->json(['text' => $success]);


    }

    public function update(Job $job, JobRequest $request){
        $success = true;
        try{
            $job->update([
                'company' => $request->input('company'),
                'begin' => $request->input('begin'),
                'end' => $request->input('end'),
                'job' => $request->input('job'),
                'description' => $request->input('description'),
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada editada con exito')]);
        }
        return response()->json(['text' => $success]);
    }


    public function destroy(Job $job){
        $success = true;
        try{
            $job->delete();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada borrada con exito')]);
        }
        return response()->json(['text' => $success]);
    }
}
