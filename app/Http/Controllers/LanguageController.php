<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LanguageRequest;
use Illuminate\Support\Facades\DB;
use App\Language;
use App\User;

class LanguageController extends Controller
{
    public function index(User $user){
        $user->load('languages');
        return view('languages/index')->with(compact('user'));
    }

    public function store(LanguageRequest $request, User $user){
        $success = true;
        try{
            DB::beginTransaction();

            Language::create([
                'name' => $request->input('name'),
                'level' => $request->input('level'),
                'center' => $request->input('center'),
                'description' => $request->input('description'),
                'user_id' => $user->id
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text'=> __('Datos almacenados')]);
        }

        return response()->json(['text' => $success]);


    }

    public function update(Language $language, LanguageRequest $request){
        $success = true;
        try{
            $language->update([
                'name' => $request->input('name'),
                'level' => $request->input('level'),
                'center' => $request->input('center'),
                'description' => $request->input('description'),
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada editada con exito')]);
        }
        return response()->json(['text' => $success]);
    }


    public function destroy(Language $language){
        $success = true;
        try{
            $language->delete();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada borrada con exito')]);
        }
        return response()->json(['text' => $success]);
    }
}
