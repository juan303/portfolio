<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMessage;

class MessageController extends Controller
{
    public function send(MessageRequest $request){


        $success = true;
        try{
            Mail::to('gdf000@hotmail.com')->send(new SendMessage($request));
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
        }
       if($success == 'true'){
           return response()->json(['text' => 'Gracias por ponerse en contacto. En breve recibirá una respuesta.']);
       }
       else{
           return response()->json(['text' => $success]);
       }

    }
}
