<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalInfoRequest;
use App\PersonalInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class PersonalInfoController extends Controller
{
    public function index(User $user){
        $user->load('personal_info');
        return view('personal_info/index')->with(compact('user'));
    }
    public function update(User $user, PersonalInfoRequest $request){
        $personal_info = $user->personal_info;
        $success = true;
        $fileName = null;
        try{
            DB::beginTransaction();
            if($request->hasFile('image')){
                $entro = "entro";
                $file = $request->file('image');
                $fileName = uniqid().$file->getClientOriginalName();
                Storage::disk('public')->delete('images/profile/'.$personal_info->image);
                if($imagePath = $file->storeAs('images', $fileName)) {
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(300, 300)->encode();
                    Storage::disk('public')->put('images/profile/'.$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                }
                $personal_info->update([
                    'image' => $fileName
                ]);
            }

            $personal_info->update([
                'name' => $request->input('name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'birthdate' => $request->input('birthdate'),
                'city' => $request->input('city'),
                'address' => $request->input('address'),
                'cp' => $request->input('cp'),
                'province' => $request->input('province'),
            ]);

        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            //return back()->with('message', ['type' => 'success', 'text' => __('Datos actualizados')]);
            return response()->json(['text'=> __('Datos actualizados'), 'image' => $personal_info->image_url]);
        }
        return response()->json(['text'=> $success]);


    }
}
