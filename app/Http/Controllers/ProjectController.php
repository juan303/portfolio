<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;

class ProjectController extends Controller
{
    public function index(User $user){
        $user->load('projects');
        return view('projects/index')->with(compact('user'));
    }

    public function store(ProjectRequest $request, User $user){
        $success = true;
        try{

            DB::beginTransaction();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('long_description'));
            $long_description = $dom->saveHTML();

            $project = Project::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'long_description' => $long_description,
                'web_page' => $request->input('web_page'),
                'user_id' => $user->id
            ]);
            dd();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text'=> __('Datos almacenados'), 'project_id' => $project->id]);
        }
        return response()->json(['text' => $success]);
    }

    public function update(Project $project, ProjectRequest $request){
        $success = true;
        try{

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('long_description'));
            $long_description = $dom->saveHTML();
            $project->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'long_description' => $long_description,
                'web_page' => $request->input('web_page'),
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada editada con exito')]);
        }
        return response()->json(['text' => $success]);
    }


    public function destroy(Project $project){
        $success = true;
        try{
            $project->delete();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada borrada con exito')]);
        }
        return response()->json(['text' => $success]);
    }
}
