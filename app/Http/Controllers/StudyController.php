<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudyRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Study;

class StudyController extends Controller
{
    public function index(User $user){
        $user->load('studies');
        return view('studies/index')->with(compact('user'));
    }

    public function store(StudyRequest $request, User $user){
        $success = true;
        try{
            DB::beginTransaction();

            Study::create([
                'center' => $request->input('center'),
                'study' => $request->input('study'),
                'begin' => $request->input('begin'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'user_id' => $user->id
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text'=> __('Datos almacenados')]);
        }

        return response()->json(['text' => $success]);


    }

    public function update(Study $study, StudyRequest $request){
        $success = true;
        try{
            $study->update([
                'center' => $request->input('center'),
                'study' => $request->input('study'),
                'begin' => $request->input('begin'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
            ]);
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada editada con exito')]);
        }
        return response()->json(['text' => $success]);
    }


    public function destroy(Study $study){
        $success = true;
        try{
            $study->delete();
        }
        catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }

        if($success === true){
            DB::commit();
            return response()->json(['text' => __('Entrada borrada con exito')]);
        }
        return response()->json(['text' => $success]);
    }
}
