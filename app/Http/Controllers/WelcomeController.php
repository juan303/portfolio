<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
        $user = User::first();
        if(!$user){
            return redirect('register');
        }
        $user->load([
            'personal_info',
            'jobs' => function($q){
                $q->select('jobs.*')->orderBy('begin', 'DESC');
            },
            'studies' => function($q){
                $q->select('studies.*')->orderBy('begin', 'DESC');
            },
            'languages',
            'projects'
        ]);
        return view('welcome')->with(compact('user'));
    }
}
