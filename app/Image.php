<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = ['image', 'project_id'];
    use SoftDeletes;

    protected static function boot(){
        parent::boot();
        static::deleting(function(Image $image){
            if(substr($image->image, 0, 4) !== 'http'){
                Storage::disk('public')->delete('images/projects/'.$image->image);
            }
        });
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function destacar($project_id){
        Image::where('project_id', $project_id)->update([
            'featured' => false,
        ]);
        $this->featured = true;
        $this->save();
    }

    //accessors
    public function getImageUrlAttribute(){
        return 'storage/images/projects/'.$this->image;
    }
}
