<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    protected $fillable = ['company', 'begin', 'end', 'job', 'description', 'user_id'];

    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
