<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    protected $fillable = ['description', 'name', 'level', 'center', 'user_id'];

    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
