<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Other extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
