<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonalInfo extends Model
{
    protected $fillable = ['name', 'last_name', 'email', 'birthdate' , 'phone', 'user_id', 'image', 'city', 'address', 'cp', 'province'];
    //protected $dates = ['birthdate'];

    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getImageUrlAttribute(){
        return 'storage/images/profile/'.$this->image;
    }

}
