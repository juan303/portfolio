<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Image;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'long_description', 'web_page', 'user_id'];
    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }

    //ACCESSOR
    public function getFeaturedImageUrlAttribute(){
        $featuredImage = $this->images()->where('featured', true)->first();
        if(!$featuredImage){
            $featuredImage = $this->images()->first();
            if(!$featuredImage){
                return 'images/projects/default.png';
            }
        }
        return $featuredImage->image_url;
    }
}
