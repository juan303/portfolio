<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Study extends Model
{
    protected $fillable = ['study', 'center', 'begin', 'end', 'description', 'user_id'];
    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
