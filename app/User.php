<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function personal_info(){
        return $this->hasOne(PersonalInfo::class);
    }
    public function jobs(){
        return $this->hasMany(Job::class);
    }
    public function studies(){
        return $this->hasMany(Study::class);
    }
    public function languages(){
        return $this->hasMany(Language::class);
    }
    public function others(){
        return $this->hasMany(Other::class);
    }
    public function projects(){
        return $this->hasMany(Project::class);
    }
}
