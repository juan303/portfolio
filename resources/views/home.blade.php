@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#contact-form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $('.form-error').css('display', 'none');
                $.post(url, $(this).serialize(), function(data){
                    $('#modal_ok').modal('show')
                }).fail(function(data){
                    var json = JSON.stringify(data);
                    var json_array = $.parseJSON(json);
                    $.each(json_array.responseJSON.errors, function(key, value){
                        $('#'+key+'_error').css('display', 'block');
                        $('#'+key+'_error').html(value);
                    });
                })
            })
        })
    </script>
@endsection

@section('content')
    <header class="header">
        @include('partials.navigations.navigation')
    </header>
    <!-- Intro Image-->
    <section id="intro" style="background: url({{ asset('img/home.jpg') }}) center center no-repeat; background-size: cover;" class="intro-section pb-2">
        <div class="container text-center">
            <div data-animate="fadeInDown" class="logo"><img src="{{ asset('img/logo-big.png') }}" alt="logo" width="130"></div>
            <h1 data-animate="fadeInDown" class="text-shadow mb-5">{{ __('Zona de configuracion de tus datos') }}</h1>
            <p data-animate="slideInUp" class="h3 text-shadow text-400">{{ __('En esta zona podrás configurar adecuadamente tus datos personales, academicos o profesionales para configurar un curriculum a tu gusto. Selecciona el apartado que quieres del menu superior para poder editar agregar, quitar o modificar informacion') }}</p>
        </div>
    </section>
    <!-- About-->



@endsection
