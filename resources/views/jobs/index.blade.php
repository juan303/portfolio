@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            //===============================================================================================EDITAR, CREAR, ELIMINAR
            $('.job_form_edit, #job_form_new, .form_delete').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var form_modal = $(this);
                var formdata = new FormData(form_modal[0]);
                var is_edit = false;
                var general_modal = $(this).parents(".modal");
                $('.form-error').css('display', 'none');
                $.ajax({
                    url: url,
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        //si NO estoy EDITANDO cierro el modal del formulario ya que si estoy EDITANDO puedo seguir queriendo editar
                        if(form_modal.data('action') !== 'edit'){
                            general_modal.modal('hide');
                        }
                        $("#modal_message .alert").attr('class', 'alert alert-success');
                        $("#modal_message .alert").html(data.text);
                        $('#modal_message').modal('show');
                        is_edit = true;
                    },
                    error: function(data){
                        var json = JSON.stringify(data);
                        var json_array = $.parseJSON(json);
                        $.each(json_array.responseJSON.errors, function(key, value){
                            $("#"+key+"_error").css('display', 'block');
                            $("#"+key+"_error").html(value);
                        })
                    }
                });
                //Si he tocado algo correctamente en la BD (editar eliminar crear) recargo la pagina al cerrar la ventana que toque
                $('#modal_message').on('hidden.bs.modal', function () {
                    if (is_edit === true && form_modal.data('action') !== 'edit') {
                        window.location.reload();
                    }
                })
                general_modal.on('hidden.bs.modal', function () {
                    if (is_edit === true && form_modal.data('action') === 'edit') {
                        window.location.reload();
                    }
                })

            });
        })
    </script>
@endsection

@section('content')
    <header class="header">
        @include('partials.navigations.navigation')
    </header>
    <!-- Intro Image-->
    <section id="" style="background: url({{ asset('img/home.jpg') }}) center center no-repeat; background-size: cover;" class="intro-section pb-2">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-md-12 bg-light shadow p-2 text-dark position-relative" >
                    <h2 class="text-shadow mb-4">{{ __('Experiencia laboral') }}</h2>
                    <button class="btn btn-lg btn-primary position-absolute shadow" style="top:-5px; right: -5px" data-toggle="modal" data-target="#new-job-modal-form" >{{ __('Agregar') }}</button>
                    @forelse($user->jobs as $job)
                        <div class="col-md-12 mt-2">
                            @include('partials.jobs.job_card')
                        </div>
                        @include('partials.messages.modals.modal_delete', ['url' => route('job.destroy', ['job' => $job->id]),
                                       'item_id' => $job->id,
                                       'message' => '¿Seguro que quiere eliminar la entrada "'.$job->id.'"'])
                        @include('partials.jobs.modals.edit_job_modal_form', ['job' => $job])
                    @empty
                        <div class="col-md-12">
                            <div class="alert alert-warning text-dark shadow p-2" data-animate="fadeInLeft">
                                {{ __('Ninguna entrada en esta sección') }}
                            </div>
                        </div>
                    @endforelse
                </div>

            </div>

        </div>
    </section>
    @include('partials.messages.modals.message')
    @include('partials.jobs.modals.new_job_modal_form')

    <!-- About-->



@endsection
