
<div class="position-absolute" style="top: 0;right: 0">
    <button type="button" data-toggle="modal" data-target="#confirm_{{ $item->id }}" rel="tooltip" title="{{ __('Eliminar entrada') }}" class="btn shadow btn-danger m-0 p-2">Eliminar</button>
    <button data-toggle="modal" data-id="{{ $item->id }}"  data-target="#edit-{{ $type }}-modal-form-{{ $item->id }}" rel="tooltip" title="{{ __('Editar') }}" class="btn shadow btn-warning m-0 p-2 text-dark" >{{ __('Editar') }}</button>

    @if(strpos(url()->current(), 'projects'))
        <button type="button" data-toggle="modal" rel="tooltip" data-id="{{ $item->id }}" title="{{ __('Agregar fotos') }}"  class="add_images_button btn shadow btn-primary m-0 p-2">Agregar fotos</button>
    @endif

    <button class="btn btn-default mr-1">Visible</button>
</div>
