<div class="card text-dark shadow p-2 text-left position-relative" >
    <div class="card-body">
        <h3>{{ $job->company }}</h3>
        <p class="mb-0 text-muted text-small"><strong>Fecha de inicio: </strong>{{ $job->begin }} | <strong>Fecha de finalización: </strong>{{ $job->end }}</p>
        <h5>{{ $job->job }}</h5>
        <p class="mb-0">{{ $job->description }}</p>
    </div>
    @if(strpos(url()->current(), 'jobs'))
        @include('partials.admin_buttons', ['item' => $job, 'type'=>'job'])
    @endif
</div>