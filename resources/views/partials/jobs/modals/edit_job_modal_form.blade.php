
<div class="modal fade text-dark" id="edit-job-modal-form-{{ $job->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="edit" class="job_form_edit" action="{{ route('job.update', ['job'=>$job->id]) }}" method="post">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h2 class="text-dark mb-2">{{ __('Editar entrada') }}: {{$job->id}}</h2>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-end">
                        <span id="company_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="company" class="col-sm-4 col-form-label">{{ __('Compañia') }}:</label>
                        <input type="text" id="company" name="company" class="form-control col-sm-8" value="{{ old('company', $job->company) }}"/>
                    </div>

                    <div class="row justify-content-end">
                        <div id="begin_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="begin" class="col-sm-4 col-form-label">{{ __('Fecha de inicio') }}:</label>
                        <input id="begin" type="date" name="begin" class="form-control col-sm-8" value={{ old('begin', $job->begin) }} />
                    </div>

                    <div class="row justify-content-end">
                        <div id="end_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="end" class="col-sm-4 col-form-label">{{ __('Fecha de finalización') }}:</label>
                        <input id="end" type="date" name="end" class="form-control col-sm-8" value={{ old('end', $job->end) }} />
                    </div>

                    <div class="row justify-content-end">
                        <div id="job_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="job" class="col-sm-4 col-form-label">{{ __('Puesto en la empresa') }}:</label>
                        <input id="job" type="text" name="job" class="form-control col-sm-8" value="{{ old('job', $job->job) }}" />
                    </div>

                    <div class="row justify-content-end">
                        <div id="description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="description" class="col-sm-4 col-form-label">{{ __('Descripción') }}:</label>
                        <textarea id="description" type="text" name="description" class="form-control col-sm-8" value="" >{{old('description', $job->description)}}</textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary m-auto" type="submit">Guardar</button>
                </div>
            </form>
        </div>

    </div>

</div>