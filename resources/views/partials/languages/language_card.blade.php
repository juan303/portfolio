<div class="card text-dark shadow p-2 text-left position-relative" >
    <div class="card-body">
        <h3 class="mr-5 mb-0 d-inline">{{ $language->name }}</h3>{{ $language->level }}
        <hr class="mt-1 mb-2" />
        @if($language->center)
        <h5><span class="text-muted">{{ __('Centro') }}:</span> {{ $language->center }}</h5>
        @endif
        <p class="mb-0">{{ $language->description }}</p>
    </div>
    @if(strpos(url()->current(), 'languages'))
        @include('partials.admin_buttons', ['item' => $language, 'type'=>'language'])
    @endif
</div>