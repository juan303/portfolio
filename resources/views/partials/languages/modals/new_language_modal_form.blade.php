
<div class="modal fade" id="new-language-modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="create" id="language_form_new" action="{{ route('language.store', ['user'=>$user->id]) }}" method="post">
                @csrf
                <div class="modal-header">
                    <h2 class="text-shadow mb-2">{{ __('Nuevo idioma') }}</h2>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-end">
                        <span id="name_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">{{ __('Idioma') }}:</label>
                        <input type="text" id="name" name="name" class="form-control col-sm-8" value="{{ old('name') }}"/>
                    </div>

                    <div class="row justify-content-end">
                        <span id="level_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="level" class="col-sm-4 col-form-label">{{ __('Nivel') }}:</label>
                        <input type="text" id="level" name="level" class="form-control col-sm-8" value="{{ old('level') }}"/>
                    </div>

                    <div class="row justify-content-end">
                        <div id="job_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="center" class="col-sm-4 col-form-label">{{ __('Centro') }}:</label>
                        <input id="center" type="text" name="center" class="form-control col-sm-8" value="{{ old('center') }}" />
                    </div>

                    <div class="row justify-content-end">
                        <div id="description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="description" class="col-sm-4 col-form-label">{{ __('Descripción') }}:</label>
                        <textarea id="description" type="text" name="description" class="form-control col-sm-8" value="" >{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary m-auto" type="submit">Guardar</button>
                </div>
            </form>
        </div>

    </div>

</div>