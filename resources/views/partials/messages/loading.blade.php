<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 3000">
    <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
        <img width="200px" src="{{ asset('images/loading/loading.gif') }}" alt="">
    </div>
</div>