<div class="modal fade" id="confirm_{{ $item_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
        <form data-action="delete" class="form_delete" action="{{ $url }}" method="post">
            @csrf
            @method('DELETE')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">¡Atencion!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <p class="mb-0">{{ $message }}</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning w-25">{{ __('SI') }}</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary w-25">{{ __('NO') }}</button>
                </div>
            </div>
        </form>
    </div>

</div>