<nav class="navbar navbar-expand-lg fixed-top">
    <div class="container"><a href="#intro" class="navbar-brand scrollTo">{{ __('Administración') }}</a>
        <button type="button" data-toggle="collapse" data-target="#navbarcollapse"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                class="navbar-toggler navbar-toggler-right"><span class="fa fa-bars"></span></button>
        <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a href="{{ route('welcome') }}" class="nav-link">{{ __('PORTFOLIO') }}</a></li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Salir') }}
                    </a>
                    <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="" class="nav-link">{{ __('Introducción') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('projects.index', ['user'=> auth()->user()->id]) }}" class="nav-link">{{ __('Proyectos') }}</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">{{ __('Servicios') }}</a>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        {{ __('Curriculum') }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="margin-top: -20px;">
                        <a href="{{ route('personal_info.index', ['user' => auth()->user()->id]) }}"
                           class="dropdown-item">{{ __('Personal') }}</a>
                        <a href="{{ route('studies.index', ['user' => auth()->user()->id]) }}"
                           class="dropdown-item">{{ __('Estudios') }}</a>
                        <a href="{{ route('jobs.index', ['user' => auth()->user()->id]) }}"
                           class="dropdown-item">{{ __('Experiencia') }}</a>
                        <a href="{{ route('languages.index', ['user' => auth()->user()->id]) }}"
                           class="dropdown-item">{{ __('Idiomas') }}</a>
                        <a href="{{ route('others.index', ['user' => auth()->user()->id]) }}"
                           class="dropdown-item">{{ __('Otros') }}</a>
                    </div>
                </li>


            </ul>
        </div>
    </div>
</nav>