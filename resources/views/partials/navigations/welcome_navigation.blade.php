<nav class="navbar navbar-expand-lg fixed-top">
    <div class="container"><a href="#intro" class="navbar-brand scrollTo">{{ __('Tecnologías Web') }}</a>
        <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><span class="fa fa-bars"></span></button>
        <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="#intro" class="nav-link link-scroll">{{ __('Introducción') }}</a></li>
                <li class="nav-item"><a href="#curriculum" class="nav-link link-scroll">{{ __('Curriculum') }}</a></li>
                <li class="nav-item"><a href="#references" class="nav-link link-scroll">{{ __('Mis proyectos') }}</a></li>
                <li class="nav-item"><a href="#services" class="nav-link link-scroll">{{ __('Servicios') }}</a></li>
                <li class="nav-item"><a href="#contact" class="nav-link link-scroll">{{ __('Contacto') }}</a></li>
            </ul>
        </div>
    </div>
</nav>