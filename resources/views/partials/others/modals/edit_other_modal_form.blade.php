
<div class="modal fade text-dark" id="edit-other-modal-form-{{ $other->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="edit" class="other_form_edit" action="{{ route('other.update', ['other'=>$other->id]) }}" method="post">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h2 class="text-dark mb-2">{{ __('Editar entrada') }}: {{$other->id}}</h2>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-end">
                        <span id="name_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">{{ __('Nombre') }}:</label>
                        <input type="text" id="name" name="name" class="form-control col-sm-8" value="{{ old('name', $other->name) }}"/>
                    </div>
                    <div class="row justify-content-end">
                        <div id="description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="description" class="col-sm-4 col-form-label">{{ __('Descripción') }}:</label>
                        <textarea id="description" type="text" name="description" class="form-control col-sm-8" value="" >{{old('description', $other->description)}}</textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary m-auto" type="submit">Guardar</button>
                </div>
            </form>
        </div>

    </div>

</div>