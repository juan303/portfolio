<div class="card text-dark shadow p-2 text-left position-relative" >
    <div class="card-body">
        <h3 class="mr-5 mb-0 d-inline">{{ $other->name }}</h3>
        <hr class="mt-1 mb-2" />
        <p class="mb-0">{{ $other->description }}</p>
    </div>
    @if(strpos(url()->current(), 'others'))
        @include('partials.admin_buttons', ['item' => $other, 'type'=>'other'])
    @endif
</div>