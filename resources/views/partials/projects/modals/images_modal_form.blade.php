

<div class="modal fade" id="modal_images" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="store-images" action="{{ route('image.store') }}" data="{{ route('image.store') }}" id="mydropzone" name="file[]" type="file" multiple class="dropzone store_images_form"  method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="project_id" id="project_id">
                <div class="dz-message" data-dz-message><span>Arrastra aquí tus imágenes</span></div>
                <div class="row justify-content-center">
                    <button id="procesar_fotos" type="button" class="btn btn-warning btn-lg">Procesar fotos</button>
                </div>
            </form>
        </div>

    </div>

</div>