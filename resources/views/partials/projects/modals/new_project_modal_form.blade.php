
<div class="modal fade" id="new-project-modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="create" id="project_form_new" action="{{ route('project.store', ['user'=>$user->id]) }}" method="post" class="form_new">
                @csrf
                <div class="modal-header">
                    <h2 class="text-shadow mb-2">{{ __('Nuevo') }}</h2>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-end">
                        <span id="name_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">{{ __('Nombre') }}:</label>
                        <input type="text" id="name" name="name" class="form-control col-sm-8" value="{{ old('name') }}"/>
                    </div>
                    <div class="row justify-content-end">
                        <div id="description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="description" class="col-sm-4 col-form-label">{{ __('Descripción') }}:</label>
                        <input type="text" id="description" name="description" class="form-control col-sm-8" value="{{ old('description') }}"/>
                    </div>
                    <div class="row justify-content-end">
                        <div id="long_description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="long_description" class="col-sm-4 col-form-label">{{ __('Descripcion larga') }}:</label>
                        <textarea id="long_description" type="text" name="long_description" class="summernote form-control col-sm-8" value="" >{!!old ('long_description') !!}</textarea>
                    </div>
                    <div class="row justify-content-end">
                        <div id="web_page_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="web_page" class="col-sm-4 col-form-label">{{ __('Página web') }}:</label>
                        <input type="text" id="web_page" name="web_page" class="form-control col-sm-8" value="{{ old('web_page') }}"/>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary m-auto" type="submit">Guardar</button>
                </div>
            </form>
        </div>

    </div>

</div>