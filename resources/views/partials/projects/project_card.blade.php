<div class="card text-dark shadow p-2 text-left position-relative" >
    <div class="card-body">
        <h3 class="mr-5 mb-2 d-inline">{{ $project->name }}</h3>
        <hr class="mt-1 mb-2" />
        <h5 class="mb-0 text-muted">{{ $project->description }}</h5>
        <hr class="mt-1 mb-2" />
        <p class="mb-0">{!! $project->long_description  !!} </p>
        <hr class="mt-1 mb-2" />
        <p class="mb-0">{{ __('Página web') }}: <span class="font-italic">{{ $project->web_page }}</span></p>
        <hr class="mt-1 mb-2" />
        @forelse($project->images as $image)
            <div class="d-inline-block position-relative" @if($image->featured == true) style="border: 2px solid blue" @endif>
                <img src="{{ asset($image->image_url) }}" style="width:80px" alt="">
                <form action="{{ route('image.destroy', ['image' => $image->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" data-toggle="modal" data-target="" rel="tooltip" title="{{ __('Eliminar entrada') }}" style="top: 0;right: 0" class="position-absolute px-1 btn shadow btn-danger m-0 px-1 py-0" >X</button>
                </form>
                @if($image->featured != true)
                    <a href="{{ route('image.feature', ['project_id' => $project->id, 'image_id' => $image->id]) }}" class="btn btn-success btn-just-icon px-1 py-0 position-absolute" style="top:0; left: 0" rel="tooltip" title="Destacar imagen">
                        <i class="fas fa-bolt"></i>
                    </a>
                @endif
            </div>
        @empty
            <p class="mb-0">{{ __('Ninguna imagen para este proyecto') }}</p>
        @endforelse
    </div>
    @if(strpos(url()->current(), 'projects'))
        @include('partials.admin_buttons', ['item' => $project, 'type'=>'project'])
    @endif
</div>