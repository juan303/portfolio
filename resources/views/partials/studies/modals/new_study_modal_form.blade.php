
<div class="modal fade" id="new-study-modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content p-3">
            <form data-action="create" id="study_form_new" action="{{ route('study.store', ['user'=>$user->id]) }}" method="post">
                @csrf
                <div class="modal-header">
                    <h2 class="text-shadow mb-2">{{ __('Nueva formación académica') }}</h2>
                </div>

                <div class="modal-body">
                    <div class="row justify-content-end">
                        <span id="study_error" class="form-error text-small text-danger"></span>
                    </div>
                    <div class="form-group row">
                        <label for="study" class="col-sm-4 col-form-label">{{ __('Titulo') }}:</label>
                        <input type="text" id="study" name="study" class="form-control col-sm-8" value="{{ old('study') }}"/>
                    </div>

                    <div class="row justify-content-end">
                        <div id="job_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="center" class="col-sm-4 col-form-label">{{ __('Centro') }}:</label>
                        <input id="center" type="text" name="center" class="form-control col-sm-8" value="{{ old('center') }}" />
                    </div>

                    <div class="row justify-content-end">
                        <div id="begin_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="begin" class="col-sm-4 col-form-label">{{ __('Fecha de inicio') }}:</label>
                        <input id="begin" type="date" name="begin" class="form-control col-sm-8" value="{{ old('begin') }}"/>
                    </div>

                    <div class="row justify-content-end">
                        <div id="end_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group row">
                        <label for="end" class="col-sm-4 col-form-label">{{ __('Fecha de finalización') }}:</label>
                        <input id="end" type="date" name="end" class="form-control col-sm-8" value="{{ old('end') }}" />
                    </div>

                    <div class="row justify-content-end">
                        <div id="description_error" class="form-error text-small text-danger"></div>
                    </div>
                    <div class="form-group  row">
                        <label for="description" class="col-sm-4 col-form-label">{{ __('Descripción') }}:</label>
                        <textarea id="description" type="text" name="description" class="form-control col-sm-8" value="" >{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary m-auto" type="submit">Guardar</button>
                </div>
            </form>
        </div>

    </div>

</div>