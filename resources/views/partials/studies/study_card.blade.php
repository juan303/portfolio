<div class="card text-dark shadow p-2 text-left position-relative" >
    <div class="card-body">
        <h3 class="mr-5 mb-0">{{ $study->study }}</h3>
        <span class="text-muted text-small">
            <strong>Fecha de inicio: </strong>{{ $study->begin }} | <strong>Fecha de finalización: </strong>{{ $study->end }}
        </span>
        <hr class="mt-1 mb-2" />
        <h5><span class="text-muted">{{ __('Centro') }}:</span> {{ $study->center }}</h5>
        <p class="mb-0">{{ $study->description }}</p>
    </div>
    @if(strpos(url()->current(), 'studies'))
        @include('partials.admin_buttons', ['item' => $study, 'type'=>'study'])
    @endif
</div>