@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#personal_info_form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var form = $(this);
                var formdata = new FormData(form[0]);
                $('.form-error').css('display', 'none');
                $.ajax({
                    url: url,
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        $("#modal_message .alert").attr('class', 'alert alert-success');
                        $("#modal_message .alert").html(data.text);
                        $('#profile-img').attr('src', '{{  url('/')."/"  }}'+data.image)
                        $('#modal_message').modal('show');

                    },
                    error: function(data){
                        var json = JSON.stringify(data);
                        var json_array = $.parseJSON(json);
                        var errors = "<ul>";
                        $.each(json_array.responseJSON.errors, function(key, value){
                            errors += "<li>"+value+"</li>";
                        });
                        errors += "</ul>";
                        $("#modal_message .alert").attr('class', 'alert alert-danger');
                        $("#modal_message .alert").html(errors);
                        $('#modal_message').modal('show');
                    }
                })
            })
        })
    </script>
@endsection

@section('content')
    <header class="header">
        @include('partials.navigations.navigation')
    </header>
    <!-- Intro Image-->
    <section id="intro" style="background: url({{ asset('img/home.jpg') }}) center center no-repeat; background-size: cover;" class="intro-section pb-2">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card text-dark shadow p-2" data-animate="fadeInLeft">
                        <form id="personal_info_form" action="{{ route('personal_info.update', ['user' => $user->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-header">
                                <h2 data-animate="fadeInDown" class="text-shadow mb-2">{{ __('Datos personales') }}</h2>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div></div>
                                    <label for="name" class="col-sm-4 col-form-label">{{ __('Nombre') }}:</label>
                                    <input type="text" id="name" name="name" class="form-control col-sm-8" value="{{ old('name', $user->personal_info->name) }}"/>
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-sm-4 col-form-label">{{ __('Apellidos') }}:</label>
                                    <input id="last_name" type="text" name="last_name" class="form-control col-sm-8" value="{{ old('last_name', $user->personal_info->last_name) }}"/>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label">{{ __('Correo electrónico') }}:</label>
                                    <input id="email" type="email" name="email" class="form-control col-sm-8" value="{{ old('email', $user->personal_info->email) }}" />
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-4 col-form-label">{{ __('Teléfono') }}:</label>
                                    <input id="phone" type="text" name="phone" class="form-control col-sm-8" value="{{ old('phone', $user->personal_info->phone) }}" />
                                </div>
                                <div class="form-group  row">
                                    <label for="birthdate" class="col-sm-4 col-form-label">{{ __('Fecha de nacimiento') }}:</label>
                                    <input id="birthdate" type="date" name="birthdate" class="form-control col-sm-8" value={{ $user->personal_info->birthdate }} />
                                </div>
                                <div class="form-group row">
                                    <label for="image" class="col-form-label col-md-2">{{ __('Foto') }}</label>
                                    <div class="input-group col-md-6">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" for="image">Buscar foto</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <img id="profile-img" style="width: 120px" class="img-thumbnail" src="{{ asset($user->personal_info->image_url) }}" alt="">
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label for="address" class="col-sm-4 col-form-label">{{ __('Dirección') }}:</label>
                                    <input id="address" type="text" name="address" class="form-control col-sm-8" value="{{ old('address', $user->personal_info->address) }}" />
                                </div>
                                <div class="form-group  row">
                                    <label for="city" class="col-sm-4 col-form-label">{{ __('Población') }}:</label>
                                    <input id="city" type="text" name="city" class="form-control col-sm-8" value="{{ old('city', $user->personal_info->city) }}" />
                                </div>
                                <div class="form-group  row">
                                    <label for="province" class="col-sm-4 col-form-label">{{ __('Provincia') }}:</label>
                                    <input id="province" type="text" name="province" class="form-control col-sm-8" value="{{ old('province', $user->personal_info->province) }}" />
                                </div>
                                <div class="form-group  row">
                                    <label for="cp" class="col-sm-4 col-form-label">{{ __('Código postal') }}:</label>
                                    <input id="cp" type="text" name="cp" class="form-control col-sm-8" value="{{ old('cp', $user->personal_info->cp) }}" />
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary" type="submit">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('partials.messages.modals.message')
    <!-- About-->



@endsection
