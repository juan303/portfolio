@extends('layouts.app')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <link href="{{ asset('css/dropzone.css') }}" rel="stylesheet" />
@endsection

@section('scripts')
    <!-- include summernote css/js -->

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script src="{{ asset('js/dropzone.js') }}" type="text/javascript"></script>

    <script>

        $(document).ready(function() {
            $('.add_images_button').click(function(e){
                e.preventDefault();
                var this_form = $(this);
                var id = this_form.data('id');
                $("#modal_images #project_id").attr('value', id)
                $("#modal_images").modal('show');
            });
        });

        $(document).ready(function () {
            //===============================================================================================EDITAR, CREAR, ELIMINAR
            crud();
        })
        function crud(){
            $('.form_edit, .form_new, .form_delete, #store_images_form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                var form_modal = $(this);
                var formdata = new FormData(form_modal[0]);
                var is_edit = false;
                var general_modal = $(this).parents(".modal");
                $('.form-error').css('display', 'none');
                $.ajax({
                    url: url,
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                        //si NO estoy EDITANDO o SUBIENDO IMAGENES cierro el modal del formulario ya que si estoy EDITANDO puedo seguir queriendo editar
                        if(form_modal.data('action') !== 'edit'){
                            general_modal.modal('hide');
                        }
                        if(form_modal.data('action') === 'create'){
                            $("#modal_images #project_id").attr('value', data.project_id)
                            $("#modal_images").modal('show');

                        }
                        else{
                            $("#modal_message .alert").attr('class', 'alert alert-success');
                            $("#modal_message .alert").html(data.text);
                            $("#modal_message").modal('show');
                        }
                        is_edit = true;
                    },
                    error: function(data){
                        var json = JSON.stringify(data);
                        var json_array = $.parseJSON(json);
                        $.each(json_array.responseJSON.errors, function(key, value){
                            $("#"+key+"_error").css('display', 'block');
                            $("#"+key+"_error").html(value);
                        })
                    }
                });
                //Si he tocado algo correctamente en la BD (editar eliminar crear) recargo la pagina al cerrar la ventana que toque
                $('#modal_message').on('hidden.bs.modal', function () {
                    if (is_edit === true && form_modal.data('action') !== 'edit' && form_modal.data('action') !== 'create') {
                        window.location.reload();
                    }
                })
                general_modal.on('hidden.bs.modal', function () {
                    if (is_edit === true && form_modal.data('action') === 'edit') {
                        window.location.reload();
                    }
                })

            });
        }


        Dropzone.autoDiscover = true;
        Dropzone.options.mydropzone = {
            addRemoveLinks: true,
            autoProcessQueue: false,
            dictRemoveFile: "Eliminar",
            uploadMultiple: true,
            parallelUploads: 10,
            maxFilesize: 8,
            init: function(file) {
                var dropz = this;
                this.on("completemultiple", function(file) {
                    $("#modal_message .alert").attr('class', 'alert alert-success');
                    $("#modal_message .alert").html('fotos subidas');
                    $("#modal_message").modal('show');
                    $('#modal_message').on('hidden.bs.modal', function () {
                        window.location.reload();
                    })
                });
                $('#procesar_fotos').click(function () {
                    dropz.processQueue();
                })

            }
        }
    </script>
@endsection

@section('content')
    <header class="header">
        @include('partials.navigations.navigation')
    </header>
    <!-- Intro Image-->
    <section id="" style="background: url({{ asset('img/home.jpg') }}) center center no-repeat; background-size: cover;" class="intro-section pb-2">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-md-12 bg-light shadow p-2 text-dark" >
                    <h2 class="text-shadow mb-2">{{ __('Proyectos') }}</h2>
                    <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#new-project-modal-form" >{{ __('Agregar') }}</button>
                    @forelse($user->projects as $project)
                        <div class="col-md-12 mt-2">
                            @include('partials.projects.project_card')
                        </div>
                        @include('partials.messages.modals.modal_delete', ['url' => route('project.destroy', ['projects' => $project->id]),
                                       'item_id' => $project->id,
                                       'message' => '¿Seguro que quiere eliminar la entrada "'.$project->id.'"'])
                        @include('partials.projects.modals.edit_project_modal_form', ['projects' => $project])
                    @empty
                        <div class="col-md-12">
                            <div class="alert alert-warning text-dark shadow p-2" data-animate="fadeInLeft">
                                {{ __('Ninguna entrada en esta sección') }}
                            </div>
                        </div>
                    @endforelse
                </div>

            </div>

        </div>
    </section>
    @include('partials.messages.modals.message')
    @include('partials.projects.modals.new_project_modal_form')
    @include('partials.projects.modals.images_modal_form')

    <!-- About-->



@endsection
