@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#contact-form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $('.form-error').css('display', 'none');
                $.post(url, $(this).serialize(), function(data){
                    $("#modal_message .alert").attr('class', 'alert alert-success');
                    $('#modal_message .alert').html(data.text);
                    $('#modal_message').modal('show')
                }).fail(function(data){
                    var json = JSON.stringify(data);
                    var json_array = $.parseJSON(json);
                    $.each(json_array.responseJSON.errors, function(key, value){
                        $('#'+key+'_error').css('display', 'block');
                        $('#'+key+'_error').html(value);
                    });
                })
            })
        })
    </script>
@endsection

@section('content')
    <header class="header">
       @include('partials.navigations.welcome_navigation')
    </header>
    <!-- Intro Image-->
    <section id="intro" style="background: url({{ asset('img/home.jpg') }}) center center no-repeat; background-size: cover;" class="intro-section pb-2">
        <div class="container text-center">
            {{--<div data-animate="fadeInDown" class="logo"><img src="{{ asset('img/logo-big.png') }}" alt="logo" width="130"></div>--}}
            <h1 data-animate="fadeInDown" class="text-shadow mb-5">{{ __('Servicios de desarrollo Web') }}</h1>
            <p data-animate="slideInUp" class="h3 text-shadow text-400">{{ __('PHP | HTML | CSS | JavaScript | Laravel...') }}</p>
        </div>
    </section>
    <!-- About-->
   <section id="curriculum" class="curriculum-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <header class="text-center">
                        <h2 data-animate="fadeInDown" class="title">{{ __('Mis ') }}</h2>
                    </header>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" class="active show nav-link text-dark" href="#home">{{ __('Información personal') }}</a></li>
                        <li><a data-toggle="tab" class="nav-link text-dark tab"  href="#menu1">{{ __('Experiencia profesional') }}</a></li>
                        <li><a data-toggle="tab" class="nav-link text-dark"  href="#menu2">{{ __('Formación académica') }}</a></li>
                        <li><a data-toggle="tab" class="nav-link text-dark"  href="#menu3">{{ __('Idiomas') }}</a></li>
                        <li><a data-toggle="tab" class="nav-link text-dark"  href="#menu4">{{ __('Otros') }}</a></li>
                    </ul>

                    <div class="tab-content mt-3">
                        <div id="home" class="tab-pane fade in active show">
                            <h3>Información personal</h3>
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="{{ asset($user->personal_info->image_url) }}" class="img-thumbnail img-fluid" alt="">

                                </div>
                                <div class="col-sm-8">
                                    <ul class="list-group">
                                        <li class="list-group-item"><span class="font-weight-bold">{{ __('Nombre') }}: </span>{{ $user->personal_info->name }}</li>
                                        <li class="list-group-item"><span class="font-weight-bold">{{ __('Apellidos') }}: </span>{{ $user->personal_info->last_name }}</li>
                                        <li class="list-group-item"><span class="font-weight-bold">{{ __('Correo electrónico') }}: </span>{{ $user->personal_info->email }}</li>
                                        <li class="list-group-item"><span class="font-weight-bold">{{ __('Telefono') }}: </span>{{ $user->personal_info->phone }}</li>
                                        <li class="list-group-item"><span class="font-weight-bold">{{ __('Fecha de nacimiento') }}: </span>{{ $user->personal_info->birthdate }}</li>
                                        <li class="list-group-item">
                                            <span class="font-weight-bold">{{ __('Dirección') }}: </span>
                                            {{ $user->personal_info->address }}, {{ $user->personal_info->city }}, {{ $user->personal_info->cp }}, {{ $user->personal_info->province }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <h3>{{ __('Experiencia profesional') }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @forelse($user->jobs as $job)
                                        @include('partials.jobs.job_card')
                                    @empty
                                        <div class="alert alert-warning">
                                            {{ __('No hay ninguna experiencia registrada') }}
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>{{ __('Formación académica') }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @forelse($user->studies as $study)
                                        @include('partials.studies.study_card')
                                    @empty
                                        <div class="alert alert-warning">
                                            {{ __('No hay ninguna experiencia registrada') }}
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>{{ __('Idiomas') }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @forelse($user->languages as $language)
                                        @include('partials.languages.language_card')
                                    @empty
                                        <div class="alert alert-warning">
                                            {{ __('No hay ningún idioma registrado') }}
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                            <h3>{{ __('Otros') }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    @forelse($user->others as $other)
                                        @include('partials.others.other_card')
                                    @empty
                                        <div class="alert alert-warning">
                                            {{ __('No hay ningúna información registrada') }}
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!-- Statistics (Suustituir por texto o imagenes)-->
   {{-- <section id="statistics" data-dir="up" style="background: url({{ asset('img/parallax.jpg') }});" class="statistics-section text-white parallax parallax">
        <div class="container">
            <div class="row showcase text-center">
                <div data-animate="fadeInUp" class="col-lg-3 col-md-6">
                    <div class="item">
                        <div class="icon"><i class="fa fa-align-justify"></i></div>
                        <h5 class="text-400 mt-4 text-uppercase"><span class="counter">120</span><br>Websites</h5>
                    </div>
                </div>
                <div data-animate="fadeInUp" class="col-lg-3 col-md-6">
                    <div class="item">
                        <div class="icon"><i class="fa fa-users"></i></div>
                        <h5 class="text-400 mt-4 text-uppercase"><span class="counter">50</span><br>Satisfied Clients</h5>
                    </div>
                </div>
                <div data-animate="fadeInUp" class="col-lg-3 col-md-6">
                    <div class="item">
                        <div class="icon"><i class="fa fa-copy"></i></div>
                        <h5 class="text-400 mt-4 text-uppercase"><span class="counter">320</span><br>projects</h5>
                    </div>
                </div>
                <div data-animate="fadeInUp" class="col-lg-3 col-md-6">
                    <div class="item">
                        <div class="icon"><i class="fa fa-font"></i></div>
                        <h5 class="text-400 mt-4 text-uppercase"><span class="counter">333</span><br>Magazines and Brochures</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="dark-mask"></div>
    </section>--}}
    {{-- Mis proyectos --}}
    <section id="references">
        <div class="container">
            <div class="col-sm-12">
                <div class="mb-5 text-center">
                    <h2 data-animate="fadeInUp" class="title">{{ __('Mis proyectos') }}</h2>
                    <p data-animate="fadeInUp" class="lead">{{ __('Esta es una sección con algunos de mis trabajos referentes al desarrollo web.') }}</p>
                </div>
               {{-- <ul id="filter" data-animate="fadeInUp">
                    <li class="active"><a href="#" data-filter="all">All</a></li>
                    <li><a href="#" data-filter="webdesign">Webdesign</a></li>
                    <li><a href="#" data-filter="seo">SEO</a></li>
                    <li><a href="#" data-filter="marketing">Marketing</a></li>
                    <li><a href="#" data-filter="other">Other</a></li>
                </ul>--}}
                <div id="detail">
                    <div class="row">
                        <div class="col-lg-10 mx-auto"><span class="close">×</span>
                            <div id="detail-slider" class="owl-carousel owl-theme"></div>
                            <div class="text-center">
                                <h1 id="detail-title" class="title"></h1>
                            </div>
                            <div id="detail-content"></div>
                        </div>
                    </div>
                </div>
                <!-- Reference detail-->
                <div id="references-masonry" data-animate="fadeInUp">
                    <div class="row">
                        @forelse($user->projects as $project)
                            <div data-category="bootstrap" class="reference-item col-lg-3 col-md-6">
                                <div class="reference"><a href="#"><img src="{{ asset($project->featured_image_url) }}" alt="" class="img-fluid">
                                        <div class="overlay">
                                            <div class="inner">
                                                <h3 class="h4 reference-title">{{ $project->name }}</h3>
                                                <p>{{ $project->description }}</p>
                                            </div>
                                        </div></a>
                                    <div data-images="
                                            @forelse($project->images as $key => $image)
                                                @if($key == 0)
                                                    {{ asset($image->image_url) }}
                                                @else
                                                    ,{{ asset($image->image_url) }}
                                                @endif
                                            @empty
                                            @endforelse
                                            " class="sr-only reference-description">
                                            {!! $project->long_description  !!}
                                        <p class="buttons text-center"><a target="_blank" href="{{ $project->web_page }}" class="btn btn-outline-primary"><i class="fa fa-globe"></i>{{ __('Visitar el sitio') }}</a></p>
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                        {{--<div data-category="bootstrap" class="reference-item col-lg-3 col-md-6">
                            <div class="reference"><a href="#"><img src="{{ asset('img/portfolio-2.jpg') }}" alt="" class="img-fluid">
                                    <div class="overlay">
                                        <div class="inner">
                                            <h3 class="h4 reference-title">Web Portfolio</h3>
                                            <p>{{ __('Portfolio desarrollado con JavaScript y Bootstrap') }}<br> [JavaScript | Bootstrap]</p>
                                        </div>
                                    </div></a>
                                <div data-images="{{ asset('img/main-slider1.jpg')}}" class="sr-only reference-description">
                                    <p>Página web diseñada mediante el Framework Laravel y Bootstrap para mostrar un curriculum personal y un portfolio</p>
                                    <p>Se ha empleado Laravel como base y bootstrap para la maquetación. También incorpora algunos plugins de jQuery para las animaciones y parte del aspecto visual.</p>
                                    <p>Adicionalmente se ha diseñado un backend para que el usuario pueda configurar todos sus datos utilizando los formularios habilidatos para ello. De esta manera podra personalizar todos los datos que necesite.</p>
                                    <p class="buttons text-center"><a target="_blank" href="http://www.juan-e-s.es" class="btn btn-outline-primary"><i class="fa fa-globe"></i>{{ __('Visitar el sitio') }}</a></p>
                                </div>
                            </div>
                        </div>
                        <div data-category="laravel" class="reference-item col-lg-3 col-md-6">
                            <div class="reference"><a href="#"><img src="{{ asset('img/portfolio-1.jpg') }}" alt="" class="img-fluid">
                                    <div class="overlay">
                                        <div class="inner">
                                            <h3 class="h4 reference-title">CustomWoods</h3>
                                            <p>{{ __('Proyecto basado en una tienda online de venta de productos decorativos') }}<br> [Laravel | Bootstrap]</p>
                                        </div>
                                    </div></a>
                                <div data-images="{{ asset('img/main-slider_2_2.jpg')}},{{asset('img/main-slider_2_1.jpg')}},{{asset('img/main-slider_2_3.jpg') }}, {{asset('img/main-slider_2_4.jpg') }}" class="sr-only reference-description">
                                    <p>Página web para negocio, desarrollada mediante Laravel 5.6.</p>
                                    <p>La web contiene elementos para poder administrar la web y dejar al administrador realizar acciones como administrar productos o categorias (crear, editar, aliminar), subir fotos para los productos (drag & drop). Adicionalmente tambien permite a los usuarios registrados llevar un control de los pedidos y de sus direcciones donde recibir los pedidos. Integra un carrito de la compra el cual se guarda hasta que se realiza la compra o el usuario decide vaciarlo.</p>
                                    <p>La web esta diseñada de modo que pueda adaptarse a cualquier tipo de negocio de manera sencilla.</p>
                                    <p class="buttons text-center"><a target="_blank" href="http://www.woodthings.es/woodthings" class="btn btn-outline-primary"><i class="fa fa-globe"></i>{{ __('Visitar el sitio') }}</a></p>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Service-->
    <section id="services" class="bg-gradient services-section">
        <div class="container">
            <header class="text-center">
                <h2 data-animate="fadeInDown" class="title">{{ __('Servicios') }}</h2>
            </header>
            <div class="row services text-center">
                <div data-animate="fadeInUp" class="col-lg-4">
                    <div class="icon"><i class="fas fa-newspaper"></i></div>
                    <h3 class="heading mb-3 text-400">{{ __('Creacion de páginas web de información') }}</h3>
                    <p class="text-left description">{{ __('Páginas web con información e imágenes sobre tu negocio incluyendo formulario de contacto y listado de servicios que se ofrecen.') }}</p>
                </div>
                <div data-animate="fadeInUp" class="col-lg-4">
                    <div class="icon"><i class="fab fa-html5"></i> <i class="fab fa-laravel"></i></div>
                    <h3 class="heading mb-3 text-400">{{ __('Desarrollo Frontend y Backend') }}</h3>
                    <p class="text-left description">{{ __('Desarrollo web completo, incluyendo base de datos para gestionar usuarios, productos, pedidos, o información importante de la web.') }}</p>
                </div>
                <div data-animate="fadeInUp" class="col-lg-4">
                    <div class="icon"><i class="fab fa-laravel"></i></div>
                    <h3 class="heading mb-3 text-400">Desarrollo mediante Framework</h3>
                    <p class="text-left description">{{ __('Utilización de los ultimos frameworks para mejorar la seguridad y el orden del código. Laravel, Bootstrap...') }}</p>
                </div>
            </div>
            <hr data-animate="fadeInUp">
            <div data-animate="fadeInUp" class="text-center">
                <p class="lead">{{ __('¿Necesitas una web a medida para tu negocio o una web personal?') }}</p>
                <p><a href="#contact" class="btn btn-outline-light link-scroll">{{ __('Contáctame') }}</a></p>
            </div>
        </div>
    </section>

    <!-- Contact-->
    <section id="contact" data-animate="bounceIn" class="contact-section contact">
        <div class="container">
            <header class="text-center">
                <h2 class="title">{{ __('Contacto') }}</h2>
            </header>
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <form id="contact-form" method="post" action="{{ route('message.send') }}" novalidate>
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="name_error" class="form-error text-small text-danger mt-0"></div>
                                    <input type="text" name="name" placeholder="{{ __('Nombre') }} *" required="required" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div id="last_name_error" class="text-small text-danger"></div>
                                    <input type="text" name="last_name" placeholder="{{ __('Apellidos') }} *" required="required" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div id="email_error" class="form-error text-small text-danger"></div>
                                    <input type="text" name="email" placeholder="{{ __('Correo electrónico ') }} *" required="required" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="phone" placeholder="{{ __('Teléfono') }}" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <div id="message_error" class="form-error text-small text-danger"></div>
                                    <textarea name="message" placeholder="{{ __('Mensaje') }} *" rows="4" required="required" class="form-control"></textarea>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-outline-primary">{{ __('Enviar mensaje') }}</button>
                                </div>
                                <div id="errors_message"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Map-->
   <div id="map"></div>
    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-lg-left">
                    <p class="social"><a href="#" class="external facebook wow fadeInUp"><i class="fa fa-facebook"></i></a><a href="#" data-wow-delay="0.2s" class="external instagram wow fadeInUp"><i class="fa fa-instagram"></i></a><a href="#" data-wow-delay="0.4s" class="external gplus wow fadeInUp"><i class="fa fa-google-plus"></i></a><a href="#" data-wow-delay="0.6s" class="email wow fadeInUp"><i class="fa fa-envelope"></i></a></p>
                </div>
                <!-- /.6-->
                <div class="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
                    <p>© 2018 Juan Esteban.</p>
                </div>

            </div>
        </div>
    </footer>
    @include('partials.messages.modals.message')
    <button type="button" data-toggle="collapse" data-target="#style-switch" aria-expanded="false" id="style-switch-button" class="btn btn-primary btn-sm hidden-xs hidden-sm"><i class="fa fa-cog fa-2x"></i></button>
    <div id="style-switch" class="collapse">
        <h4 class="text-uppercase">{{ __('Selecciona el color del tema') }}</h4>
        <form class="mb-3">
            <select name="colour" id="colour" class="form-control">
                <option value="">{{ __('Seleccionar variante de color') }}</option>
                <option value="default">{{ __('tuequesa') }}</option>
                <option value="red">{{ __('rojo') }}</option>
                <option value="pink">{{ __('rosa') }}</option>
                <option value="green">{{ __('verde') }}</option>
                <option value="violet">{{ __('violeta') }}</option>
                <option value="sea">{{ __('azul marino') }}</option>
                <option value="blue">{{ __('azul') }}</option>
            </select>
        </form>
        <p><img src="img/template-mac.png" alt="" class="img-fluid"></p>
        <p class="text-muted text-small">Stylesheet switching is done via JavaScript and can cause a blink while page loads. This will not happen in your production code.</p>
    </div>


@endsection
