<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();




//Mail Message
Route::post('message', 'MessageController@send')->name('message.send');

Route::group(['middleware' => ['auth']], function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'personal_info'], function(){
        Route::get('/{user}', 'PersonalInfoController@index')->name('personal_info.index');
        Route::put('/{user}/update', 'PersonalInfoController@update')->name('personal_info.update');
    });
    Route::group(['prefix' => 'jobs'], function(){
        Route::get('/{user}', 'JobController@index')->name('jobs.index');
        Route::post('/{user}/store', 'JobController@store')->name('job.store');
        Route::put('/{job}/update', 'JobController@update')->name('job.update');
        Route::delete('/{job}/destroy', 'JobController@destroy')->name('job.destroy');
    });
    Route::group(['prefix' => 'studies'], function(){
        Route::get('/{user}', 'StudyController@index')->name('studies.index');
        Route::post('/{user}/store', 'StudyController@store')->name('study.store');
        Route::put('/{study}/update', 'StudyController@update')->name('study.update');
        Route::delete('/{study}/destroy', 'StudyController@destroy')->name('study.destroy');
    });
    Route::group(['prefix' => 'languages'], function(){
        Route::get('/{user}', 'LanguageController@index')->name('languages.index');
        Route::post('/{user}/store', 'LanguageController@store')->name('language.store');
        Route::put('/{language}/update', 'LanguageController@update')->name('language.update');
        Route::delete('/{language}/destroy', 'LanguageController@destroy')->name('language.destroy');
    });
    Route::group(['prefix' => 'others'], function(){
        Route::get('/{user}', 'OtherController@index')->name('others.index');
        Route::post('/{user}/store', 'OtherController@store')->name('other.store');
        Route::put('/{other}/update', 'OtherController@update')->name('other.update');
        Route::delete('/{other}/destroy', 'OtherController@destroy')->name('other.destroy');
    });
    Route::group(['prefix' => 'projects'], function(){
        Route::get('/{user}', 'ProjectController@index')->name('projects.index');
        Route::post('/{user}/store', 'ProjectController@store')->name('project.store');
        Route::put('/{project}/update', 'ProjectController@update')->name('project.update');
        Route::delete('/{project}/destroy', 'ProjectController@destroy')->name('project.destroy');
    });
    Route::group(['prefix' => 'images'], function(){
        Route::get('/{projects}', 'ImageController@index')->name('images.index');
        Route::post('/store', 'ImageController@store')->name('image.store');
        Route::delete('/{image}/destroy', 'ImageController@destroy')->name('image.destroy');
        Route::get('/{project_id}/{image_id}', 'ImageController@feature')->name('image.feature'); //destacar imagen
    });
});